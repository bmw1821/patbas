from flask import Flask

app = Flask(__name__)


@app.route('/test')
def test():
    return 'success!', 200


if __name__ == '__main__':
    app.run('0.0.0.0', 5001, debug=False)
